mkdir tvbaqp
cd tvbaqp/
echo "## Downloading TVBAQP ##"
wget https://bitbucket.org/jlalueza/tvbaqp/get/master.tar.gz
echo "## Unpacking ##"
tar -xzf master.tar.gz
echo "## Moving files ##"
cp -r jlalueza-tvbaqp-*/* .
echo "## Cleaning ##"
rm -rf jlalueza-tvbaqp-*/
rm -f master.tar.gz
echo "## Done ##"
