#!/bin/bash

# Remove dependencies for TVBAQualityProbe execution

# Check if root
if [[ $EUID -ne 0 ]]; then
   echo "[ERROR] This script must be run as root" 
   exit 1
fi

rm -rf /usr/local/lib/TVBAQP/
rm -rf /etc/ld.so.conf.d/tvbaqp.conf

dependencies=( libopencv-dev build-essential cmake git libgtk2.0-dev pkg-config python-dev python-numpy libdc1394-22 libdc1394-22-dev libjpeg-dev libpng12-dev libtiff4-dev libjasper-dev libavcodec-dev libavformat-dev libswscale-dev libxine-dev libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev libv4l-dev libtbb-dev libqt4-dev libfaac-dev libmp3lame-dev libopencore-amrnb-dev libopencore-amrwb-dev libtheora-dev libvorbis-dev libxvidcore-dev x264 v4l-utils unzip libtiff5-dev libxine2-dev libqt5opengl5-dev libqt5opengl5-dev libqt5test5 )

# Remove software dependencies
remove_dependencies(){
	for dep in "${dependencies[@]}"; do
		if [[ ! -z $(apt-cache policy ${dep} | grep "Installed: (none)") ]]; then
			echo "[DEBUG] Removing ${dep}"
			apt-get -y remove ${dep}
			echo "[DEBUG] Removed ${dep}"
		fi
	done
}

remove_dependencies

# Remove libraries from cache
ldconfig
