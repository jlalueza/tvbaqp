#!/bin/bash

# Install dependencies for TVBAQualityProbe execution

# Check if root
if [[ $EUID -ne 0 ]]; then
   echo "[ERROR] This script must be run as root" 
   exit 1
fi

LIBS_DIR=/usr/local/lib/TVBAQP

# Create directory for TVBAQP libraries
if [[ ! -d ${LIBS_DIR} ]]; then
	mkdir ${LIBS_DIR}
	echo "[INFO] TVBAQP directory lib created"
fi

# Copy file to add the TVBAQP libraries to the Libraries Path
if [[ ! -e /etc/ld.so.conf.d/tvbaqp.conf ]]; then
	cp tvbaqp.conf /etc/ld.so.conf.d/
	echo "[INFO] Copied tvbaqp.conf"
fi

dependencies=( libopencv-dev build-essential cmake git libgtk2.0-dev pkg-config python-dev python-numpy libdc1394-22 libdc1394-22-dev libjpeg-dev libpng12-dev libtiff4-dev libjasper-dev libavcodec-dev libavformat-dev libswscale-dev libxine-dev libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev libv4l-dev libtbb-dev libqt4-dev libfaac-dev libmp3lame-dev libopencore-amrnb-dev libopencore-amrwb-dev libtheora-dev libvorbis-dev libxvidcore-dev x264 v4l-utils unzip libtiff5-dev libxine2-dev libqt5opengl5-dev libqt5opengl5-dev libqt5test5 )

# Install software dependencies
install_dependencies(){
	for dep in "${dependencies[@]}"; do
		if [[ ! -z $(apt-cache policy ${dep} | grep "Installed: (none)") ]]; then
			echo "[DEBUG] Installing ${dep}"
			apt-get -y install ${dep}
			echo "[DEBUG] Installed ${dep}"
		fi
	done
}

# Copy libraries dependencies
copy_libraries(){
	local target_root=$1
	local source_root=$(pwd)
	for lib in *; do
		if [[ -d ${lib} ]]; then
			cd ${lib}
			if [[ ! -e "${target_root}/${lib}" ]]; then
				mkdir "${target_root}/${lib}"
			fi
			copy_libraries "${target_root}/${lib}"
			continue
		elif [[ -f ${lib} ]]; then
			if [[ ! -f "${target_root}/${lib}" ]]; then
				cp ${lib} "${target_root}/"
				chown root:root "${target_root}/${lib}"
				chmod 644 "${target_root}/${lib}"
				echo "[INFO] Copied ${lib} to ${target_root}/"
			else
				echo "[DEBUG] Library ${lib} already in system"
			fi
		else
			echo "[DEBUG] ${lib} is not a file or directory"
		fi
		echo "[DEBUG] Creating symlinks for ${target_root}/${lib} if needed"
		cd ${target_root}
		case ${lib} in
		libopencv*)
			if [[ ! -h ${lib%.0} || ( ! -h ${lib%.3.2.0} ) ]]; then
				ln -sf ${lib} ${lib%.0}
				ln -sf ${lib%.0} ${lib%.3.2.0}
				echo "[INFO] Symlinks ${lib%.0} and ${lib%.3.2.0} created"
			fi
		;;
		esac
		cd ${source_root}
		echo "[INFO] Installed ${lib} library"
	done
}

if [[ -d Dependencies/ ]]; then # && ( ! -z "$(ls -A libs/)"
	cd Dependencies/
	apt-get update
	install_dependencies
	copy_libraries ${LIBS_DIR}
fi

# Copy libraries to cache
ldconfig
